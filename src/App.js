import './sass/manifest.scss';
import IndexState from "./state";
import IndexLayout from "./layout/layout";

const App = () => {

    return (
        <IndexState>
            <IndexLayout />
        </IndexState>
    );
}

export default App;

import React from "react";
import GlobalState from "./global";

const IndexState = props => {
    return <GlobalState>{props.children}</GlobalState>;
};

export default IndexState;

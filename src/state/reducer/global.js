const StateInitial = {
    recipients: [],
    filteredRecipients: [],
    tables:{},

    columns: {
        date: true,
        name: true,
        address: true,
    },
};

const StateReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_RECIPIENTS':
            let recipients = state.recipients;
            recipients.push(action.payload.recipient)
            state = {
                ...state,
                recipients:recipients,
                filteredRecipients:recipients
            };
            break;

        case 'FILTER_RECIPIENTS':
            state = {
                ...state,
                filteredRecipients: action.payload.filteredRecipients ?? [],
            };
            break;
        case 'SET_RECIPIENTS':
            state = {
                ...state,
                recipients: action.payload.recipients ?? [],
                filteredRecipients: action.payload.recipients ?? [],
            };
            break;
        case 'SET_COLUMNS':{
            let tables = state.tables;
            tables[action.payload.tableId] = action.payload.hideColumns
            state = {
                ...state,
                tables:tables
            };
            break;
        }

        case 'SET_COLUMN':{
            let tables = state.tables;
            tables[action.payload.tableId][action.payload.column] = action.payload.value;
            state = {
                ...state,
                tables:tables
            };
            break;
        }
        default:
            break;
    }

    return state;
};

export {StateReducer, StateInitial};

import React, { useReducer, createContext } from "react";
import { StateInitial, StateReducer } from "./reducer/global";

const GlobalStateContext = createContext([{}]);

const GlobalState = props => {
    const [state, dispatch] = useReducer(StateReducer, StateInitial);

    return (
        <GlobalStateContext.Provider value={[state, dispatch]}>
            {props.children}
        </GlobalStateContext.Provider>
    );
};

export default GlobalState;
export { GlobalStateContext };

import React, {useContext, useState} from 'react';
import fetch from 'node-fetch';
import {GlobalStateContext} from "../state/global";
import {dateFormat, getBaseUrl} from "../helpers/helpers";

const AddUserForm = ({closeModal}) => {
    const [globalState, globalDispatch] = useContext(GlobalStateContext);
    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const formReady = () => {
        return name.length > 0 && address.length > 0;
    }
    const addRecipient = async () => {
        if (formReady()) {

            const params = new URLSearchParams();

            params.append('name', name);
            params.append('address', address);
            params.append('date', dateFormat(new Date()));

            const response = await fetch(`${getBaseUrl()}/address`, {method: 'POST', body: params});
            const data = await response.json();

            globalDispatch({
                type: "SET_RECIPIENTS",
                payload: {
                    recipients: data.response
                },
            });
            setName("");
            setAddress("");
        }
    }

    return (
        <Modal closeModal={() => closeModal()}>
            <div className="add-user-form-component">
                <div className="top">
                    <div className="title">Add User</div>
                    <div className="close" onClick={() => closeModal()}>✕</div>
                </div>
                <form>
                    <div className="form-input">
                        <label>Name</label>
                        <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>
                    </div>
                    <div className="form-input">
                        <label>Address</label>
                        <input type="text" value={address}
                                              onChange={(e) => setAddress(e.target.value)}/>
                    </div>
                </form>
                <div className="bottom">
                    <button className={formReady() ? 'hover':''} onClick={() => addRecipient()}>Confirm</button>
                    <button onClick={() => closeModal()}>Cancel</button>
                </div>
            </div>
        </Modal>
    );
};

const Modal = ({children, closeModal}) => {
    return (
        <div className="modal" onClick={() => closeModal()}>
            <div className="modal-box" onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>
    )
}

export default AddUserForm;

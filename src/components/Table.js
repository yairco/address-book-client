import React, {useContext, useEffect, useState} from "react";
import Columns from "./Columns";
import {GlobalStateContext} from "../state/global";

export const Table = ({filteredRecipients, children, id}) => {
    const [recipients, setRecipients] = useState(filteredRecipients ?? [])
    const [tableId, setTableId] = useState(id ?? false)
    const [globalState, globalDispatch] = useContext(GlobalStateContext);

    useEffect(()=>{

        if(id){
            globalDispatch({
                type: "SET_COLUMNS",
                payload: {
                    tableId: tableId,
                    hideColumns: {
                        date: false,
                        name: false,
                        address: false,
                    }
                },
            });
        }

    },[id])

    const hideColumn = (columnName) => {
        try{
            return globalState.tables[id][columnName];
        }catch (e){
            return false;
        }
    }


    useEffect(() => {
        setRecipients(filteredRecipients);
    }, [filteredRecipients])

    return (
        <div className="table-component">
            <div className="widgets">
                <Columns id={id}/>
                {children}
            </div>
            <div className="table">
                <table>
                    <thead>
                    <tr>
                        <th hidden={hideColumn("date")}>Date</th>
                        <th hidden={hideColumn("name")}>Name</th>
                        <th hidden={hideColumn("address")}>Address</th>
                    </tr>
                    </thead>
                    <tbody>
                    {recipients ? recipients.map((recipient, index) => {
                        return (
                            <tr key={index}>
                                <td hidden={hideColumn("date")}>{recipient.date}</td>
                                <td hidden={hideColumn("name")}>{recipient.name}</td>
                                <td hidden={hideColumn("address")}>{recipient.address}</td>
                            </tr>
                        )
                    }) : <tr></tr>}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

import React, {useContext} from 'react';
import {GlobalStateContext} from "../state/global";


const Columns = ({id}) => {
    const [globalState, globalDispatch] = useContext(GlobalStateContext);

    return (
        <div className="filters">
            <Column title="date" checked={globalState.tables[id]?.date ?? false} tableId={id}/>
            <Column title="name" checked={globalState.tables[id]?.name ?? false} tableId={id}/>
            <Column title="address" checked={globalState.tables[id]?.address ?? false} tableId={id}/>
        </div>
    );
};

const Column = (props) => {
    const [globalState, globalDispatch] = useContext(GlobalStateContext);

    const updateColumn = (name, value, tableId) => {
        globalDispatch({
            type: "SET_COLUMN",
            payload: {
                column: name,
                value: value,
                tableId: tableId
            },
        })
    }

    return (
        <div className="column-component">
            <label>
                <input type="checkbox"
                       checked={props.checked}
                       onChange={(e) => updateColumn(props.title, e.target.checked, props.tableId)}
                />{props.title}</label>
        </div>
    )
}

export default Columns;

import React, {useState, useContext, useEffect} from "react";
import {Table} from "../components/Table";
import {GlobalStateContext} from "../state/global";
import fetch from 'node-fetch';
import AddUserForm from "../components/AddUserForm";
import {getBaseUrl} from '../helpers/helpers';

const IndexLayout = (props) => {
    const [globalState, globalDispatch] = useContext(GlobalStateContext);
    const [recipients, setRecipients] = useState([]);
    const [query, setQuery] = useState("");
    const [showForm, setShowForm] = useState(false);


    useEffect(async () => {
        const response = await fetch(`${getBaseUrl()}/address`);
        const data = await response.json();

        globalDispatch({
            type: "SET_RECIPIENTS",
            payload: {
                recipients: data.response
            },
        })

    }, [])

    useEffect(() => {
        setRecipients(globalState.recipients)
    }, [globalState.recipients])

    useEffect(() => {

        if (!recipients||recipients.length < 1) {
            return;
        }
        if(query.length < 1){
            return;
        }

        const timeoutID = setTimeout(() => {
            let q = query.toLowerCase();
            let filtered = recipients.filter(recipient => {
                return Object.keys(recipient).some(key => {
                    return recipient[key].toLowerCase().includes(q)
                });
            })

            globalDispatch({
                type: "FILTER_RECIPIENTS",
                payload: {
                    filteredRecipients: filtered
                },
            })
        }, 100);
        return () => clearTimeout(timeoutID);
    }, [query])

    return (
        <>
            {showForm ? <AddUserForm closeModal={()=>setShowForm(false)} /> : ""}
            <div className="container">
                <Table filteredRecipients={globalState.filteredRecipients} key="table-1" id="table1">
                    <div className="search-add-component">
                        <input type="text" className="search" placeholder="Search Table" onChange={(e) => setQuery(e.target.value)}/>
                        <button onClick={()=>{setShowForm(true)}} className="add-user hover">Add User</button>
                    </div>
                </Table>
                <Table filteredRecipients={globalState.recipients} key="table-2" id="table2"/>
            </div>
        </>
    );
};

export default IndexLayout;